<?php
function ubah_huruf($string){
	$input = strtolower($string);
	$huruf = [];
	foreach (range("a","z") as $a) {
		array_push($huruf, $a);
	}
	$hasil = [];

	for ($i=0; $i < strlen($input); $i++) { 
		for ($j=0; $j < count($huruf); $j++) { 
			if ($input[$i] == $huruf[$j]) {
				array_push($hasil, $huruf[$j+1]);
			}
		}
	}
	$hasil = implode("", $hasil);
	return $hasil."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>